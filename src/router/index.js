import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    //login
    {
        path: '/login',
        name: 'login',
        component: () =>
            import ('../views/Login.vue')
    },
    //register
    {
        path: '/register',
        name: 'register',
        component: () =>
            import ('../views/Register.vue')
    },
    //accueil
    {
        path: '/accueil',
        name: 'Home',
        component: () =>
            import ('../views/Home.vue'),
        meta: {
            requiresAuth: false
        }
    },
    //newSubmission
    {
        path: '/newsubmission',
        name: 'newsubmission',
        component: () =>
            import ('../views//NewSubmission.vue'),
        meta: {
            requiresAuth: true
        }
    },
    //detail submission
    {
        path: '/detailsubmission/:id',
        name: 'detailsubmission',
        component: () =>
            import ('../views/DetailSubmission.vue'),
        meta: {
            requiresAuth: false
        },
        props: true
    },
    //make reveiw 
    {
        path: '/review/:id',
        name: 'review',
        component: () =>
            import ('../views/Review.vue'),
        meta: {
            requiresAuth: false
        },
        props: true
    },

    //users
    {
        path: '/users',
        name: 'users',
        component: () =>
            import ('../views/users/Users.vue'),
        meta: {
            requiresAuth: true
        },
        props: true,
        children: [{
                path: 'admins',
                component: () =>
                    import ('../views/users/Admins.vue'),
            },
            {
                path: 'reviewers',
                component: () =>
                    import ('../views/users/Reviewers.vue'),
            },
            {
                path: 'clients',
                component: () =>
                    import ('../views/users/Clients.vue'),
            },
            {
                path: 'detail/:adminId',
                component: () =>
                    import ('../views/users/Details.vue'),
            }
        ]
    },

    // Routage links Web site
    {
        path: '/em',
        name: 'devenir-membre',
        component: () =>
            import ('../views/EtreMembre.vue'),
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/about',
        name: 'about',
        component: () =>
            import ('../views/About.vue'),
        meta: {
            requiresAuth: false
        }
    },

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {

    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!localStorage.getItem('token')) {
            next({ path: '/login' })
        } else {
            if (to.path == "/") {
                next({ path: '/accueil' });
            } else {
                next();
            }
        }
    } else {
        if (to.path == "/") {
            next({ path: '/accueil' });
        } else {
            next();
        }
    }
})

export default router